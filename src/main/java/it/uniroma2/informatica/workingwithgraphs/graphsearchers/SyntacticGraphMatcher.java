package it.uniroma2.informatica.workingwithgraphs.graphsearchers;

import it.uniroma2.informatica.workingwithgraphs.graphs.SyntacticGraph;
import it.uniroma2.informatica.workingwithgraphs.graphs.Variable;

import java.util.List;
import java.util.Vector;

public class SyntacticGraphMatcher {
	
	
	
	public List<Variable> match(SyntacticGraph g, SyntacticGraph subg) {
		// TODO
		return null;
		
	}
	
	public List<List<Variable>> match(List<SyntacticGraph> gs, SyntacticGraph subg) {
		
		List<List<Variable>> answers = new Vector<List<Variable>>();
		for (SyntacticGraph g:gs) {
			List<Variable> matchedVariables = match(g,subg);
			if (matchedVariables!=null) {
				answers.add(matchedVariables);
			}
		}
		return answers;
		
	}

}
