package it.uniroma2.informatica.workingwithgraphs.graphs;

public class Relation {
	Node in; 
	Node out;
	String label;
	
	public Relation(Node in, Node out, String label) {
		this.in = in;
		this.out = out;
		this.label = label;
	}

	
}
