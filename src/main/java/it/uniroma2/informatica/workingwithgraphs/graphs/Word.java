package it.uniroma2.informatica.workingwithgraphs.graphs;

public class Word extends Node {

	String surface;


	public Word(String surface) {
		super();
		this.surface = surface;
	}
	
	public String getSurface() {
		return surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}

	
	

}
